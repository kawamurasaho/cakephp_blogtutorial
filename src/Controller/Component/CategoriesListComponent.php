<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * CategoriesList component
 */
class CategoriesListComponent extends Component
{
    public function categoriesList()
    {
        //Tableの宣言
        $Categories = TableRegistry::get('Categories');
        
        //category listの取得
        $query = $Categories->find()
            ->order(['Categories.lft' => 'ASC']);
        return $query;
    }
}
