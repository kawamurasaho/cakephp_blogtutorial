<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Admin Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles */
class AdminController extends AppController
{
    //コンポーネントの宣言
    public $components = ['Paginator', 'CategoriesList'];

    /**
    * paginate基本設定
    */
    public $helper = ['Paginator'];
    public $paginate = [
        'limit' => 10,
        'order' => ['created' => 'desc'],
        'sortWhitelist' => ['created']
    ];

    /**
     * Index method
     *
     * indexページの表示項目所得
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null
     */
    public function index($id = null)
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');
        
        //記事一覧取得
        $query = $Articles->find('all')
            ->contain(['Categories'])
            ->order(['Articles.created' => 'DESC']);
        $articles = $this->paginate($query);
        
        $this->set(compact('categories', 'articles'));
    }

    /**
     * articleAdd method
     *
     * 入力データをArticlesテーブルに格納
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function articleAdd()
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');

        $article = $Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $Articles->patchEntity($article, $this->request->data);
            if ($Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to ass your article.'));
        }
        
        //Just added the categories list to be able to choose
        //one category for an article
        $categories = $Articles->Categories->find('treeList');
        $this->set(compact('article','categories'));
    }

    /**
     * articleEdit method
     *
     * 入力データを当該IDの記事データへ更新
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function articleEdit($id = null)
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');

        $article = $Articles->get($id, [
            'contain' => ['Categories']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $Articles->patchEntity($article, $this->request->data);
            if ($Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        
        $categories = $Articles->Categories->find('treeList');
        $this->set(compact('article','categories'));
    }

    /**
     * articleDelete method
     *
     * 当該IDの記事データを削除
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function articleDelete($id = null)
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');

        $this->request->allowMethod(['post', 'delete']);
        $article = $Articles->get($id);
        if ($Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * categoriesIndex method
     *
     * カテゴリ一覧を取得
     * @return \Cake\Network\Response|null
     */
    public function categoriesList()
    {
        //Tabelの宣言
        $Categories = TableRegistry::get('Categories');
        //カテゴリ一覧取得
        $categories = $Categories->find()->order(['lft' => 'ASC']);
        $categories = $this->paginate($categories);
        $this->set('categories', $categories);
    }

    /**
     * categoryAdd method
     * 入力データをcategoriesテーブルに格納
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function categoryAdd()
    {
        //Tabelの宣言
        $Categories = TableRegistry::get('Categories');
        
        $category = $Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $Categories->patchEntity($category, $this->request->data);
            if ($Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'categoriesList']);
            } else {
                $this->Flash->error(__('The category could not be saved. Please, try again.'));
            }
        }
        $parentCategories = $Categories->ParentCategories->find('list', ['limit' => 200]);
        $this->set(compact('category', 'parentCategories'));
        $this->set('_serialize', ['category']);
    }

    /**
     * categoryEdit method
     *
     * 入力データを当該IDのカテゴリデータへ更新
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function categoryEdit($id = null)
    {
        //Tabelの宣言
        $Categories = TableRegistry::get('Categories');
        
        $category = $Categories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $Categories->patchEntity($category, $this->request->data);
            if ($Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'categoriesList']);
            } else {
                $this->Flash->error(__('The category could not be saved. Please, try again.'));
            }
        }
        $parentCategories = $Categories->ParentCategories->find('list', ['limit' => 200]);
        $this->set(compact('category', 'parentCategories'));
        $this->set('_serialize', ['category']);
    }

    /**
     * categoryDelete method
     *
     * 当該IDのカテゴリデータを削除
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function categoryDelete($id = null)
    {
        //Tabelの宣言
        $Categories = TableRegistry::get('Categories');
        
        $this->request->allowMethod(['post', 'delete']);
        $category = $Categories->get($id);
        if ($Categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'categoriesList']);
    }

    /**
    * commentsList method
    *
    * コメント一覧取得
    * @return \Cake\Network\Response|null
    */
    public function commentsList()
    {
        //Tabelの宣言
        $Comments = TableRegistry::get('Comments');
        
        $comments = $Comments->find('all')
            ->andWhere(function($exp, $q){
                return $exp->isNull('Comments.parent_id');
            })
            ->contain(['ChildComments', 'Articles'])
            ->order(['Comments.created' => 'DESC']);
        $comments = $this->paginate($comments);
        $this->set('comments', $comments);
    }

    /**
     * commentAdd method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function commentAdd($commentId = null, $articleId = null)
    {
        //Tabelの宣言
        $Comments = TableRegistry::get('Comments');

        $comment = $Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $Comments->patchEntity($comment, $this->request->data);
            debug($comment);
            if ($Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect(['action' => 'commentsList']);
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        
        ;
        
        $this->set(compact('comment','commentId', 'articleId'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * commentDelete method
     *
     * 当該IDのコメントを削除
     * 親コメントだった場合、子コメントも削除
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function commentDelete($id = null)
    {
        //Tabelの宣言
        $Comments = TableRegistry::get('Comments');
        
        $this->request->allowMethod(['post', 'delete']);
        $comment = $Comments->get($id);
        if ($Comments->delete($comment)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'commentsList']);
    }
}
?>