<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
* Views Controller
*/
class ViewsController extends AppController
{
    //コンポーネントの宣言
    public $components = ['Paginator', 'CategoriesList'];

    /**
    * paginate基本設定
    */
    public $helper = ['Paginator'];
    public $paginate = [
        'limit' => 4,
        'order' => ['Articles.created' => 'desc'],
        'sortWhitelist' => ['created']
    ];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }
    /**
    * Tags method
    *
    * 条件ごとに記事を検索
    */
    public function tags()
    {
        $tags = $this->request->params['pass'];
        $customFinderOptions = [
            'tags' => $tags
        ];
        $this->paginate = [
            'finder' => [
                'tagged' => $customFinderOptions
            ]
        ];
        $articles = $this->paginate($this->Articles);
        $this->set(compact('articles', 'tags'));
    }

    /**
     * Index method
     *
     * indexページの表示項目所得
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null
     */
    public function index($id = null)
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');
        $Comments = TableRegistry::get('Comments');
        
        //カテゴリリスト取得
        $categories = $this->CategoriesList->categoriesList();

        //記事一覧取得
        $query = $Articles->find('all')->order(['created' => 'DESC']);
        
        // カテゴリごとに記事をフィルタリング
        if(!empty($id)) {
            $conditions = (["Articles.category_id" => $id]);
            $query = $Articles->find('all', (['conditions' => $conditions]))->order(['created' => 'DESC']);
        }
        $articles = $this->paginate($query);
        
        $this->set(compact('categories', 'articles'));
    }

    /**
     * articleView method
     *
     * 当該IDの記事データを取得
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function articleView($id = null)
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');
        $Comments = TableRegistry::get('Comments');
        
        //カテゴリリスト取得
        $categories = $this->CategoriesList->categoriesList();
        
        //記事情報取得
        $article = $Articles->get($id, [
            'contain' => ['Categories']
        ]);
        
        //コメント取得
        $comments = $Comments->find('all')
            ->where(['Comments.article_id' => $id])
            ->andWhere(function ($exp, $q){
                return $exp->isNull('Comments.parent_id');
            })
            ->contain(['ChildComments'])
            ->order(['Comments.created' => 'DESC']);
        
        //コメントの追加
        $comment_entity = $Articles->Comments->newEntity($this->request->data);
        
        $this->set(compact('categories','article','comments','comment_entity'));
    }

    /**
     * commentsView method
     *
     * @param Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function commentsView($id = null)
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');
        $Comments = TableRegistry::get('Comments');
        
        //カテゴリリスト取得
        $categories = $this->CategoriesList->categoriesList();
        
        //記事タイトル取得
        $article = $Articles->get($id,[
            'select' => ['article.id', 'articles.title']
        ]);
        
        //コメント取得
        $comments = $Comments->find('all')
            ->where(['Comments.article_id' => $id])
            ->andWhere(function($exp, $q){
                return $exp->isNull('Comments.parent_id');
            })
            ->contain(['ChildComments'])
            ->order(['Comments.created' => 'desc']);
        
        //コメントの追加
        $comment_entity = $Comments->newEntity($this->request->data);
        
        //値のセット
        $this->set(compact('categories','article','comments','comment_entity'));
    }

    /**
     * commentAdd method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function commentAdd()
    {
        //Tabelの宣言
        $Articles = TableRegistry::get('Articles');
        $Comments = TableRegistry::get('Comments');

        $comment = $Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $Comments->patchEntity($comment, $this->request->data);
            if ($Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        
        //前ページへ遷移
        $url = $this->referer(['action' => 'articleView', $this->request->data['article_id']]);
        return $this->redirect($url);
        
        $articles = $Articles->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'articles'));
        $this->set('_serialize', ['comment']);
    }
}
?>