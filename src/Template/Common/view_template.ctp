<!-- page template -->
<!-- File: src/Template/Common/view_template.ctp -->
<?= $this->Html->css('page.css'); ?>
<?= $this->Html->script('jquery-3.1.1.min.js'); ?>
<?= $this->Html->script('bootstrap.min.js'); ?>

<!-- page header -->
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Blog Articles</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
               <li><?= $this->Html->link('Sign up', ['controller' => 'Users', 'action' => 'add']) ?></li>
                <li><?= $this->Html->link('Log in', ['controller' => 'Users', 'action' => 'login']) ?></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
       <!-- side nav -->
        <nav class="col-md-4">
            <div id="side-nav">
                <!-- categories list -->
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Categories List</h3></div>
                    <?php if(!empty($categories)): ?>
                        <div class="list-group">
                            <?= $this->Html->link(
                                $this->Html->tag('span', __('all')).$this->cell("Counter::display_article"),
                                ['action' => 'index'], ['class' => 'list-group-item', 'escape' => false]
                            ) ?>
                            <?php foreach($categories as $category): ?>
                                <?= $this->Html->link(
                                    $this->Html->tag('span', $category->name).$this->Html->tag('span', $this->cell("Counter::display_category", [$category->id]), ['class' => 'badge']),
                                    ['action' => 'index', $category->id], ['class' => 'list-group-item', 'escape' => false]
                                ) ?>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </nav>
        
        <!-- main contents -->
        <div class="col-md-8">
            <?= $this->fetch('main_content') ?>
        </div>
    </div>
    <!-- pager -->
    <div class="row">
       <div class=" col-md-12">
            <?= $this->fetch('pager') ?>
        </div>
    </div>
</div>