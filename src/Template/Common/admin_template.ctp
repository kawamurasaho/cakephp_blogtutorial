<!-- edit page template -->
<!-- File: src/Template/Common/admin_template.ctp -->

<?= $this->Html->css('page.css'); ?>
<?= $this->Html->script('jquery-3.1.1.min.js'); ?>
<?= $this->Html->script('bootstrap.min.js'); ?>

<!-- page header -->
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin Pages</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><?= $this->Html->link('Log out', ['controller' => 'Users', 'action' => 'logout']) ?></li>
            </ul>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
       <!-- side nav -->
        <nav class="col-md-3">
            
            <?= $this->fetch('sub_menu') ?>
            
            <!-- actions menu -->
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Contents List</h3></div>
               <div class="list-group">
                   <?= $this->Html->link(__('Articles list'), ['action' => 'index'], ['class' => 'list-group-item']) ?>
                   <?= $this->Html->link(__('Categories list'), ['action' => 'categoriesList'], ['class' => 'list-group-item']) ?>
                   <?= $this->Html->link(__('Comments list'), ['action' => 'commentsList'], ['class' => 'list-group-item']) ?>
               </div>
            </div>
        </nav>
        
        <!-- main contents -->
        <div class="col-md-9">
            <?= $this->fetch('main_content') ?>
        </div>
    </div>
</div>