<!-- 返信フォーム -->
<fieldset>
    <?php
        echo $this->Form->hidden('article_id', [
            'value' => $articleId
        ]);
        echo $this->Form->hidden('parent_id', [
            'value' => $commentId,
            'empty' => null
        ]);
        echo $this->Form->input('title',[
            'value' => 'No title'
        ]);
        echo $this->Form->input('body');
    ?>
</fieldset>
<?= $this->Form->button('Submit', ['class' => 'pull-right']); ?>