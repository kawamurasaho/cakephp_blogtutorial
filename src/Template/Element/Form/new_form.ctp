<!-- 新規コメントフォーム -->
<div class="comment_add">
    <?= $this->Form->create($comment_entity, ['url' =>['action' => 'commentAdd']]) ?>
        <fieldset>
            <legend><?= __('New Comment') ?></legend>
            <?php
                echo $this->Form->hidden('article_id', ['value' => $article->id]);
                echo $this->Form->hidden('parent_id', [
                    'value' => null,
                ]);
                echo $this->Form->input('title',[
                    'value' => 'No title'
                ]);
                echo $this->Form->input('body');
            ?>
        </fieldset>
        <?= $this->Form->button('Submit', ['class' => 'pull-right']) ?>
    <?= $this->Form->end() ?>
</div>