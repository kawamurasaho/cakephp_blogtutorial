<!-- comments list -->
<div class="comments_list">
    <?php foreach($comments as $comment): ?>
            <div class="panel panel-default comment_panel">
                <div class="panel-body">
                    <p><strong><?= h($comment->title) ?></strong></p>
                    <p><?= nl2br(h($comment->body)) ?></p>
                    <p class="text-right"><small>Created: <?= $comment->created->format(DATE_RFC850) ?></small></p>
                    <!-- 返信件数 -->
                    <p class="text-right"><a data-toggle="collapse" data-target="#rc<?= $comment->id ?>">▼返信件数:
                        <?= $this->cell("Counter::display_reply", [$comment->id])->render(); ?></a>
                    </p>
                    <!-- 返信コメント -->
                    <?php if(!empty($comment->child_comments)): ?>
                        <div id="rc<?= $comment->id ?>" class="collapse">
                            <?php foreach($comment->child_comments as $childComments): ?>
                                <div class="panel panel-default replyComments">
                                    <div class="panel-body">
                                        <p><strong><?= h($childComments->title) ?></strong></p>
                                        <p><?= nl2br(h($childComments->body)) ?></p>
                                        <p class="text-right">
                                            <small>Created: <?= $childComments->created->format(DATE_RFC850) ?></small>
                                        </p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <button type="button" class="btn btn-default pull-right" data-toggle="collapse" data-target="#<?= $comment->id ?>">
                        <?= __('返信') ?>
                    </button>
                    <!-- 返信用フォーム -->
                    <div id="<?= $comment->id ?>" class="collapse reply">
                       <?= $this->Form->create($comment_entity,
                            ['url' =>['action' => 'commentAdd']])
                        ?>
                            <?= $this->element('Form/reply_form',[
                                "commentId" => $comment->id,
                                "articleId" => $article->id
                            ]) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
    <?php endforeach; ?>
</div>