<!-- ログイン -->
<?= $this->Html->css('page.css'); ?>
<?= $this->Html->css('userForm.css'); ?>

<div class="container">
    <div class="row forms well">
        <div class="panel panel-default col-md-6">
           <div class="panel-body">
                <?= $this->Form->create() ?>
                    <fieldset>
                        <legend><?= __('Login') ?></legend>
                        <?php
                            echo $this->Form->input('name');
                            echo $this->Form->input('password');
                        ?>
                    </fieldset>
                    <?= $this->Form->button('Login', ['class' => 'pull-right']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="col-md-6">
            <?= $this->Html->link($this->Html->tag('span', '新規登録'), ['action' => 'add'],['class' => 'btn btn-default btn-lg', 'escape' => false]) ?>
        </div>
    </div>
</div>