<!-- ユーザ登録 -->
<?= $this->Html->css('page.css'); ?>
<?= $this->Html->css('userForm.css'); ?>

<div class="container">
    <div class="forms well">
        <div class="panel panel-default userForm">
           <div class="panel-body">
                <?= $this->Form->create() ?>
                    <fieldset>
                        <legend><?= __('Sign up') ?></legend>
                        <?php
                            echo $this->Form->input('name');
                            echo $this->Form->input('password');
                            echo $this->Form->hidden('status_flg',[
                                'value' => 1
                            ]);
                        ?>
                    </fieldset>
                    <?= $this->Form->button('Sign up', ['class' => 'pull-right']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>