<!-- File: src/Template/Admin/category_edit.ctp -->

<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');
?>
<?= $this->Form->create($category) ?>
    <fieldset>
        <legend><?= __('Edit Category') ?></legend>
        <?php
            echo $this->Form->input('parent_id', [
                'options' => $parentCategories,
                'empty' => 'No parent category'
            ]);
            echo $this->Form->input('name');
            echo $this->Form->input('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>

<?php $this->end(); ?>
