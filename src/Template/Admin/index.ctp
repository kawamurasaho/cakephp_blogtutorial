<!-- File://src/Template/Admin/index.ctp -->
<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');
?>
    <h3><?= __('Articles List') ?></h3>
    
    <p><?= $this->Html->link('New Articles', ['action' => 'articleAdd']) ?></p>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>Category Name</th>
                <th>Created</th>
                <th>Modified</th>
                <th>Reply count</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($articles as $article): ?>
            <tr>
                <td><?= h($article->title) ?></td>
                <td>
                    <?php if(!empty($article->category->name)):?>
                        <?= h($article->category->name) ?>
                    <?php endif; ?>
                </td>
                <td><?= h($article->created->format(DATE_RFC850)) ?></td>
                <td><?= h($article->modified->format(DATE_RFC850)) ?></td>
                <td><?= $this->cell("Counter::display_comment", [$article->id]) ?></td>
                <td class="actions">
                    <div class="btn-group-vertical">
                        <?= $this->Html->link($this->Html->tag('span', 'Edit'), ['action' => 'articleEdit', $article->id],['class' => 'btn btn-default', 'escape' => false]) ?>
                        <?= $this->Form->postLink($this->Html->tag('span', 'Delete'), ['action' => 'articleDelete', $article->id],
                                ['confirm' => __('Are you sure you want to delete # {0}?', $article->id), 'class' => 'btn btn-default', 'escape' => false]) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    
    <nav aria-label="ページ送り">
        <ul class="pagination">
            <?= $this->Paginator->numbers(
                ['before' => $this->Paginator->prev('<<'),
                 'after' => $this->Paginator->next('>>')]
            ); ?>
        </ul>
    </nav>
<?php $this->end(); ?>