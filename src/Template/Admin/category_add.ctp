<!-- File:src/Template/Admin/category_add.ctp -->

<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');
?>
<h3>Add Category</h3>
<?= $this->Form->create($category) ?>
<fieldset>
    <?php
        echo $this->Form->input('parent_id', [
            'options' => $parentCategories,
            'empty' => 'No parent category'
        ]);
        echo $this->Form->input('name');
        echo $this->Form->input('description');
    ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end(); ?>

<?php $this->end() ?>


