<!-- File: src/Template/Admin/comment_add.ctp -->

<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');

    $this->Form->create($comment,
        ['url' =>['action' => 'commentAdd']]);
        echo $this->element('Form/reply_form',[
                "commentId" => $commentId,
                "articleId" => $articleId
        ]);
    $this->Form->end();

    $this->end();
?>
