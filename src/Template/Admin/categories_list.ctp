<!-- File://src/Template/Admin/categories_list.ctp -->

<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');
?>
    <h3><?= __('Categories List') ?></h3>
    <p><?= $this->Html->link('New Categories', ['action' => 'categoryAdd']) ?></p>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Created</th>
                <th>Articles count</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($categories as $category): ?>
            <tr>
                <td><?= h($category->name) ?></td>
                <td><?= h($category->description) ?></td>
                <td><?= h($category->created->format(DATE_RFC850)) ?></td>
                <td><?= $this->cell("Counter::display_category", [$category->id]) ?>件</td>
                <td class="actions">
                    <div class="btn-group-vertical">
                        <?= $this->Html->link($this->Html->tag('span', 'Edit'), ['action' => 'categoryEdit', $category->id],['class' => 'btn btn-default', 'escape' => false]) ?>
                        <?= $this->Form->postLink($this->Html->tag('span', 'Delete'), ['action' => 'categoryDelete', $category->id],
                                ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'class' => 'btn btn-default', 'escape' => false]) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    
    <nav aria-label="ページ送り">
        <ul class="pagination">
            <?= $this->Paginator->numbers(
                ['before' => $this->Paginator->prev('<<'),
                 'after' => $this->Paginator->next('>>')]
            ); ?>
        </ul>
    </nav>
<?php $this->end(); ?>