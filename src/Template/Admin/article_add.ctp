<!-- File: src/Template/Admin/article_add.ctp -->

<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');
?>
<h3>Add Article</h3>
<?php
    echo $this->Form->create($article);
    //just added the categories input
    echo $this->Form->input('category_id');
    echo $this->Form->input('title');
    echo $this->Form->input('body', ['rows' => '10']);
    echo $this->Form->button(__('save Article'));
    echo $this->Form->end();

    $this->end();
?>