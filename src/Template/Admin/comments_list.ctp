<!-- File: src/Template/Admin/comments_list.ctp -->

<?php
    $this->extend('/Common/admin_template');
    $this->start('main_content');
?>
    <h3><?= __('Comments List') ?></h3>
    <?php foreach($comments as $comment): ?>
        <div class="panel panel-default comment_panel">
            <div class="panel-body">
                <p><strong><?= h($comment->title) ?></strong></p>
                <p><?= nl2br(h($comment->body)) ?></p>
                <p class="text-right"><small>Created: <?= $comment->created->format(DATE_RFC850) ?></small></p>
                <p class="text-right"><small>Article: <?= $comment->article->title ?></small></p>
                <!-- 返信件数 -->
                <p class="text-right"><a data-toggle="collapse" data-target="#rc<?= $comment->id ?>">▼返信件数:
                    <?= $this->cell("Counter::display_reply", [$comment->id])->render(); ?></a>
                </p>
                <!-- 返信コメント -->
                <?php if(!empty($comment->child_comments)): ?>
                    <div id="rc<?= $comment->id ?>" class="collapse">
                        <?php foreach($comment->child_comments as $childComments): ?>
                            <div class="panel panel-default replyComments">
                                <div class="panel-body">
                                    <p><strong><?= h($childComments->title) ?></strong></p>
                                    <p><?= nl2br(h($childComments->body)) ?></p>
                                    <p class="text-right">
                                        <small>Created: <?= $childComments->created->format(DATE_RFC850) ?></small>
                                    </p>
                                    <?= $this->Form->postLink($this->Html->tag('span', 'Delete'), ['action' => 'commentDelete', $comment->id],
                                        ['confirm' => __('Are you sure you want to delete # {0}?', $comment->id),
                                         'class' => 'btn btn-default pull-right', 'escape' => false])
                                    ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?= $this->Form->postLink($this->Html->tag('span', '削除'), ['action' => 'commentDelete', $comment->id],
                    ['confirm' => __('Are you sure you want to delete # {0}?', $comment->id),
                    'class' => 'btn btn-default pull-right', 'escape' => false])
                ?>
                <button type="button" class="btn btn-default pull-right" data-toggle="collapse" data-target="#<?= $comment->id ?>">
                    <?= __('返信') ?>
                </button>
                <!-- 返信用フォーム -->
                <div id="<?= $comment->id ?>" class="collapse reply">
                    <?= $this->Form->create($comment,
                        ['url' =>['action' => 'commentAdd']])
                    ?>
                        <?= $this->element('Form/reply_form',[
                            "commentId" => $comment->id,
                            "articleId" => $comment->article->id
                        ]) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    
    <nav aria-label="ページ送り">
        <ul class="pagination">
            <?= $this->Paginator->numbers(
                ['before' => $this->Paginator->prev('<<'),
                 'after' => $this->Paginator->next('>>')]
            ); ?>
        </ul>
    </nav>
<?php $this->end(); ?>