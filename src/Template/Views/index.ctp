<!-- File: src/Template/Views/index.ctp -->
<?php
    $this->extend('/Common/view_template');
    $this->start('main_content');
?>

<!-- ここから $articlesのクエリオブジェクトをループして、投稿記事の情報を表示 -->
<?php if(!empty($articles)): ?>
    <?php foreach($articles as $article): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $this->Html->link(h($article->title), ['action' => 'articleView', $article->id]) ?></h3>
            </div>
            <div class="panel-body">
                <p><?= $this->Text->truncate(
                        nl2br(h($article->body)), 20,
                        ['ellipsis' => ' ......', 'html' => 'false']
                    );?>
                </p>
            </div>
            <div class="panel-footer">
                <p class="text-right"><?= h($article->created->format(DATE_RFC850)) ?></p>
                <p class="text-right"><?= $this->Html->link('コメント数:'.$this->cell("Counter::display_comment", [$article->id]), ['action' => 'commentsView', $article->id]) ?></p>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?php
    $this->end();
    $this->start('pager');
?>
<nav aria-label="ページ送り">
    <ul class="pagination">
        <?= $this->Paginator->numbers(
            ['before' => $this->Paginator->prev('<<'),
             'after' => $this->Paginator->next('>>')]
        ); ?>
    </ul>
</nav>
<?php $this->end(); ?>
