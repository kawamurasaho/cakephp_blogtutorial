<!-- File: src/Template/Views/article_view.ctp -->
<?php
    $this->extend('/Common/view_template');
    $this->start('main_content');
?>

<!-- 記事詳細 -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($article->title) ?></h3>
    </div>
    <div class="panel-body">
        <p><?= nl2br(h($article->body)) ?></p>
    </div>
    <div class="panel-footer">
        <?php if(!empty($article->category->name)): ?>
            <p class="text-right"><small><?= h('Categories: '.$article->category->name) ?></small></p>
        <?php endif; ?>
        <p class="text-right"><small><?= h('Created: ').$article->created->format(DATE_RFC850) ?></small></p>
    </div>
</div>

<!-- comments -->
<div class="comments">
    <!-- コメント一覧 -->
    <?php if(!empty($comments)){
        echo $this->element('panel_comment');
    }?>

    <!-- new comment -->
    <?= $this->element('Form/new_form') ?>
</div>

<?php $this->end(); ?>