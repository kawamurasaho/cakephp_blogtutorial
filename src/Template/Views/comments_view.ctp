<!-- File: src/Template/Views/comments_view.ctp -->
<?php
    $this->extend('/Common/view_template');
    $this->start('main_content');
?>
<h3><?= $this->Html->link(h($article->title), ['action' => 'articleView', $article->id]) ?></h3>

<!-- comments -->
<div class="comments">
    <!-- コメント一覧 -->
    <?php if(!empty($comments)){
        echo $this->element('panel_comment');
    }?>

    <!-- new comment -->
    <?= $this->element('Form/new_form') ?>
</div>

<?php $this->end(); ?>