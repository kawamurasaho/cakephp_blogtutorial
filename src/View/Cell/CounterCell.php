<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * Counter cell
 */
class CounterCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * 返信件数を表示
     */
    public function display_reply($id = null)
    {
        //countフィールドを再構築する
        $comments = TableRegistry::get('Comments');
        $comments->recover();
        
        //返信件数の取得
        $node = $comments->get($id);
        $reply_count = $comments->childCount($node);
        $this->set('reply_count', $reply_count);
    }

    /**
    *コメント数を表示
    */
    public function display_comment($id = null)
    {
        //Tableの宣言
        $Comments = TableRegistry::get('Comments');
        
        //コメント数取得
        $query = $Comments->find('all')
            ->where(['Comments.article_id' => $id]);
        $comments_count = $query->count();
        $this->set('comments_count', $comments_count);
    }
    
    /**
    *全記事数表示
    */
    public function display_article()
    {
        //Tableの宣言
        $Articles = TableRegistry::get('Articles');
        
        //記事数取得
        $query = $Articles->find('all');
        $articles_count = $query->count();
        $this->set('articles_count', $articles_count);
    }
    
    /**
    *カテゴリごとの記事件数表示
    */
    public function display_category($id = null)
    {
        //Tableの宣言
        $Articles = TableRegistry::get('Articles');
        
        //記事数取得
        $query = $Articles->find('all')
            ->where(['Articles.category_id' => $id]);
        $categories_count = $query->count();
        $this->set('categories_count', $categories_count);
    }
}