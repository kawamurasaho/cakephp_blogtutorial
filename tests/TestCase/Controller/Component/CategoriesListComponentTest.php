<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\CategoriesListComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\CategoriesListComponent Test Case
 */
class CategoriesListComponentTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Controller\Component\CategoriesListComponent     */
    public $CategoriesList;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();        $this->CategoriesList = new CategoriesListComponent($registry);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CategoriesList);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
